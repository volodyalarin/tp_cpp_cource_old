#include "include/proccess_worker.h"

#include <semaphore.h>
#include <stdio.h>
#include <sys/file.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <unistd.h>

#include "include/io.h"
#include "include/runner.h"

#define free_all_memory()                                      \
  do {                                                         \
    if (rating) munmap(rating, sizeof(*rating) * RATING_SIZE); \
    if (cur_file) munmap(cur_file, sizeof(*cur_file));         \
    if (sem_file) sem_close(sem_file);                         \
    if (sem_rating) sem_close(sem_rating);                     \
  } while (0)

#define handle_error(code, message)                 \
  do {                                              \
    if (!process_number) free_all_memory();         \
    if (params->log_level) printf("%s\n", message); \
    exit(code);                                     \
  } while (0)

int proccess_worker(params_t *params, struct dirent **file_list,
                    int number_of_files, rating_t main_rating) {
  int child_pid = 0, process_number = 0;
  sem_t *sem_file = NULL, *sem_rating = NULL;

  size_t *cur_file = mmap(NULL, sizeof(size_t), PROT_RW, MAP_SH_AN, -1, 0);
  rating_t rating =
      mmap(NULL, sizeof(*rating) * RATING_SIZE, PROT_RW, MAP_SH_AN, -1, 0);

  if (!rating || !cur_file)
    handle_error(EXIT_FAILURE, "Не удалось выделить общую память.");

  sem_file = sem_open("semaphore_file", O_CREAT | O_EXCL, 0, 1);
  sem_unlink("semaphore_file");

  sem_rating = sem_open("semaphore_rating", O_CREAT | O_EXCL, 0, 1);
  sem_unlink("semaphore_rating");

  if (!sem_file || !sem_rating)
    handle_error(EXIT_FAILURE, "Не удалось создать семафоры.");

  for (size_t i = 1; i < params->proceses; i++) {
    child_pid = fork();

    if (child_pid == -1)
      handle_error(EXIT_FAILURE, "Не удалось создать процесс.");

    if (!child_pid)
      process_number++;
    else
      break;
  }

  if (params->log_level)
    printf("Начал процесс #%d pid %d\n", process_number, getpid());

  while (*cur_file < number_of_files) {
    int rc = runner(params, cur_file, file_list, rating, sem_file, sem_rating);
    if (rc) handle_error(rc, "Произошла ошибка раннера.");
  }

  int status = EXIT_SUCCESS;
  if (child_pid) waitpid(child_pid, &status, 0);

  if (process_number) {
    if (params->log_level)
      printf("Закончил процесс #%d pid %d\n", process_number, getpid());
    exit(EXIT_SUCCESS);
  }

  for (size_t i = 0; i < RATING_SIZE; i++) main_rating[i] = rating[i];
  free_all_memory();

  return status;
}
