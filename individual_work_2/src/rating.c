#include "include/rating.h"

#include <stdbool.h>
#include <string.h>

/**
 * @brief Добавить запись в топ рейтинг
 *
 * @param buffer буффер с топом рейтинга
 * @param rating вставляемая запись
 * @return int
 */
int push_to_rating(rating_t buffer, const rating_entity_t* rating) {
  if (!buffer || !rating) return 1;

  bool is_bigger = false;
  size_t position;

  for (position = 0; position < RATING_SIZE && !is_bigger; position++)
    if (buffer[position].score < rating->score) is_bigger = true;

  if (!is_bigger) return 0;

  position--;

  for (size_t i = RATING_SIZE - 1; i > position; i--) {
    buffer[i] = buffer[i - 1];
  }

  buffer[position] = *rating;

  return 0;
}
