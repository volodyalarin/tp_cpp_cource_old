#include "include/io.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/file.h>

/**
 * @brief Напечать рейтинг файлов
 *
 * @param rating объект рейтинга
 * @return int код ошибки
 */
int print_rating(rating_t rating) {
  if (!rating) return EXIT_FAILURE;

  printf("Результаты поиска: \n");
  for (size_t i = 0; i < RATING_SIZE && rating[i].score; i++)
    printf("%zu. Файл: %s. %zu совпадений.\n", i + 1, rating[i].filename,
           rating[i].score);

  if (!rating[0].score) printf("Совпадений не найдено.\n");

  return EXIT_SUCCESS;
}

/**
 * @brief Получить полный путь до файла
 *
 * Конкатирует путь до папки и само имя файла
 *
 * @param dirname папка с файлом
 * @param filename имя файла
 * @return char* полный пыть до файла
 */
char *get_path(const char *dirname, const char *filename) {
  if (!dirname || !filename) return NULL;

  size_t size = strlen(dirname) + strlen(filename) + 2;

  char *str = calloc(size, sizeof(char));
  if (!str) return NULL;

  size_t work_pos = snprintf(str, size, "%s", dirname);
  if (dirname[strlen(dirname)] != '/')
    work_pos += snprintf(str + work_pos, size - work_pos, "/");
  work_pos += snprintf(str + work_pos, size - work_pos, "%s", filename);

  return str;
}

/**
 * @brief Восвобождает список файлов в директории
 *
 * @param file_list список файлов
 * @param number_of_files колличество файлов
 */
void free_file_list(struct dirent **file_list, int number_of_files) {
  if (!file_list) return;

  while (number_of_files--) {
    free(file_list[number_of_files]);
  }
  free(file_list);
}

/**
 * @brief Напечать подсказку по запуску приложения
 *
 */
void show_help() {
  help_option_t options[] = {
      {"help", 'h', "Показать помощь", 0},
      {"processes", 'p', "Установить принудительно колличество процессов.", 0},
      {"directory", 'd', "Директория, в который будет произведен поиск.", 0},
      {"query", 'q', "Поисковый запрос", 1},
      {"log-level", 'l', "Установить уровень отладочных сообщений.", 0},
      {0, 0, 0, 0}};

  help_option_t *current = options;

  while (current->long_name) {
    printf("--%s -%c - ", current->long_name, current->short_name);
    if (current->is_required)
      printf("обязательный ключ\n");
    else
      printf("необязательный ключ\n");

    printf("%s\n", current->description);
    current++;
  }
}
