#include "include/runner.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include "include/file_parse.h"
#include "include/io.h"

int runner(params_t *params, size_t *cur_file, struct dirent **file_list,
           rating_t rating, sem_t *sem_file, sem_t *sem_rating) {
  if (!params || !cur_file || !file_list || !rating) return EXIT_FAILURE;

  if (sem_file) sem_wait(sem_file);

  int cur = *cur_file;
  (*cur_file)++;

  if (sem_file) sem_post(sem_file);

  if (params->log_level >= 2)
    printf("LOG: PID%d Process file %s FILE #%d\n", getpid(),
           file_list[cur]->d_name, cur);

  char *filename = get_path(params->dirname, file_list[cur]->d_name);

  ssize_t score =
      get_words_occurrence(filename, params->words, params->words_count);

  if (params->log_level >= 2)
    printf("LOG: PID%d Processed file %s with score %zu\n", getpid(), filename,
           score);

  if (score < 0) {
    return EXIT_FAILURE;
  }

  rating_entity_t result = {.score = score};
  strncpy(result.filename, file_list[cur]->d_name, sizeof(result.filename));

  if (sem_rating) sem_wait(sem_rating);
  push_to_rating(rating, &result);
  if (sem_rating) sem_post(sem_rating);

  free(filename);
}
