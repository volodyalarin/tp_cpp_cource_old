#include <dirent.h>
#include <dlfcn.h>
#include <semaphore.h>
#include <stdio.h>
#include <sys/file.h>

#include "include/default_worker.h"
#include "include/io.h"
#include "include/params.h"
#include "include/proccess_worker.h"

#define free_all_memory()                                      \
  do {                                                         \
    if (params) free_params(params);                           \
    if (file_list) free_file_list(file_list, number_of_files); \
  } while (0)

#define handle_error(code, message)                 \
  do {                                              \
    free_all_memory();                              \
    if (params->log_level) printf("%s\n", message); \
    return (code);                                  \
  } while (0)

#define PROT_RW PROT_READ | PROT_WRITE
#define MAP_SH_AN MAP_SHARED | MAP_ANONYMOUS

int main(size_t argc, char **argv) {
  struct dirent **file_list = NULL;
  int number_of_files = 0;
  rating_t rating = NULL;

  params_t *params = get_params(argc, argv);
  if (!params) return EXIT_FAILURE;

  if (params->help) {
    show_help();
    free_all_memory();
    return EXIT_SUCCESS;
  }

  if (params->log_level)
    printf("Для папки %s. Используется %zu процессов\n\n", params->dirname,
           params->proceses);

  number_of_files = scandir(params->dirname, &file_list, NULL, alphasort);
  if (number_of_files <= 0)
    handle_error(EXIT_FAILURE, "Не удалось получить список файлов.");

  rating = calloc(RATING_SIZE, sizeof(*rating));
  if (!rating)
    handle_error(EXIT_FAILURE, "Не удалось иницилизировать рейтинг.");

  if (params->proceses > 1) {
    void *dynlib_worker = dlopen("./libproccess_worker.so", RTLD_LAZY);
    if (!dynlib_worker) handle_error(EXIT_FAILURE, dlerror());

    int (*proccess_worker)(params_t * params, struct dirent * *file_list,
                           int number_of_files, rating_t rating) =
        dlsym(dynlib_worker, "proccess_worker");

    char *error = dlerror();
    if (error || !proccess_worker) {
      dlclose(dynlib_worker);
      handle_error(EXIT_FAILURE, error);
    }

    int rc = proccess_worker(params, file_list, number_of_files, rating);
    dlclose(dynlib_worker);
    if (rc) handle_error(rc, "Ошибка параллельного воркера");

  } else {
    int rc = default_worker(params, file_list, number_of_files, rating);
    if (rc) handle_error(rc, "Ошибка последовательного воркера");
  }

  print_rating(rating);

  free_all_memory();

  return EXIT_SUCCESS;
}
