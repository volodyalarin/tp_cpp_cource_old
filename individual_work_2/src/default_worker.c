#include "include/default_worker.h"

#include <stdio.h>

#include "include/runner.h"

#define handle_error(code, message)                 \
  do {                                              \
    if (params->log_level) printf("%s\n", message); \
    exit(code);                                     \
  } while (0)

int default_worker(params_t *params, struct dirent **file_list,
                   int number_of_files, rating_t rating) {
  for (size_t cur_file = 0; cur_file < number_of_files;) {
    int rc = runner(params, &cur_file, file_list, rating, NULL, NULL);
    if (rc) handle_error(rc, "Произошла ошибка раннера.");
  }

  return EXIT_SUCCESS;
}
