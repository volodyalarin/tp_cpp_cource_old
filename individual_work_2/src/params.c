#include "include/params.h"

#include <ctype.h>
#include <getopt.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <sys/sysinfo.h>

#include "include/io.h"

/**
 * @brief Сформировать структуру с параметрами запуска приложения
 *
 * @param argc количество аргументов командной строки
 * @param argv массив аргументов командной строки
 * @return params_t* параметры запуска
 */
params_t *get_params(size_t argc, char **argv) {
  if (!argv) return NULL;

  optind = 0;

  params_t *params = calloc(sizeof(*params), 1);

  if (!params) return NULL;
  params->proceses = get_nprocs();
  struct option options[] = {{"help", no_argument, NULL, 'h'},
                             {"processes", required_argument, NULL, 'p'},
                             {"query", required_argument, NULL, 'q'},
                             {"log-level", required_argument, NULL, 'l'},
                             {"directory", required_argument, NULL, 'd'},
                             {NULL, 0, NULL, 0}};

  int c = 0, opt_idx = 0;

  while ((c = getopt_long(argc, argv, "q:d:p:l:h", options, &opt_idx)) != -1) {
    switch (c) {
      case 'q':
        params->words = get_words(optarg, &(params->words_count));
        if (!params->words) free_params(params);
        if (!params->words) return NULL;

        break;
      case 'd':
        params->dirname = strdup(optarg);
        if (!params->dirname) free_params(params);
        if (!params->dirname) return NULL;

        break;

      case 'p':
        if (sscanf(optarg, "%zu", &(params->proceses)) != 1) params->help = 1;
        break;

      case 'l':
        if (sscanf(optarg, "%zu", &(params->log_level)) != 1) params->help = 1;
        break;

      case ':':
      case 'h':
      case '?':
        params->help = 1;
        break;
    }
  }
  if (!params->words || !params->words_count || !params->dirname)
    params->help = 1;
  if (optind < argc) params->help = 1;

  return params;
}

/**
 * @brief Высвободить структуру параметров запуска
 *
 * @param params Параметры запуска
 */
void free_params(params_t *params) {
  if (!params) return;

  free(params->dirname);
  free_words(params->words, params->words_count);
  free(params);
}

/**
 * @brief Получить список слов в строке
 *
 * @param line строка
 * @param count количество найденных слов
 * @return char** массив слов
 */
char **get_words(char *line, size_t *count) {
  if (!line || !count) return NULL;

  bool is_word = false;
  *count = 0;

  for (char *i_ptr = line; *i_ptr; i_ptr++) {
    if (isspace(*i_ptr)) {
      if (is_word) {
        is_word = false;
        (*count)++;
      }
    } else {
      is_word = true;
    }
  }
  if (is_word) (*count)++;

  char **words = calloc(sizeof(*words), *count);

  if (!words) return NULL;

  char *word_starts = NULL;
  size_t cur_word_index = 0;

  for (char *i_ptr = line; *i_ptr; i_ptr++) {
    if (isspace(*i_ptr)) {
      if (word_starts) {
        words[cur_word_index] = strndup(word_starts, i_ptr - word_starts);
        if (!words[cur_word_index]) free_words(words, *count);
        if (!words[cur_word_index]) return NULL;

        word_starts = NULL;
        cur_word_index++;
      }
    } else if (!word_starts) {
      word_starts = i_ptr;
    }
  }
  if (word_starts) {
    words[cur_word_index] = strdup(word_starts);
    if (!words[cur_word_index]) free_words(words, *count);
    if (!words[cur_word_index]) return NULL;
  }

  return words;
}

/**
 * @brief Освобождения массива слов
 *
 * @param words массив слов
 * @param count колличество слов
 */
void free_words(char **words, size_t count) {
  if (!words) return;
  for (size_t i = 0; i < count; i++) free(words[i]);
  free(words);
}
