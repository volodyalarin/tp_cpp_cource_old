#include "include/file_parse.h"

#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

/**
 * @brief Получить очередное слово из файла
 *
 * @param file файл
 * @return char* полученное слово
 */
char *get_word(FILE *file) {
  char word[WORD_MAX_SIZE];

  int code = fscanf(file, WORD_SPECIFICATOR, word);

  if (code != 1) return NULL;

  return strdup(word);
}

/**
 * @brief Получить количество вхождений слов в файл
 *
 * @param filename имя файла
 * @param words массив слов
 * @param count количество слов
 * @return ssize_t колличество вхождений
 */
ssize_t get_words_occurrence(const char *filename, char **words, size_t count) {
  if (!filename || !words || !count) return -1;

  size_t occurrence = 0;
  FILE *file = fopen(filename, "r");
  if (!file) return -1;
  char *word = NULL;
  while (word = get_word(file)) {
    bool is_coincided = false;
    for (size_t i = 0; i < count && !is_coincided; i++)
      is_coincided = !strcasecmp(word, words[i]);
    if (is_coincided) occurrence++;

    free(word);
  }
  if (file) fclose(file);

  return occurrence;
}
