#include "include/rating.h"

#include <gtest/gtest.h>

#define ASSERT_EQ_RATING(a, b)            \
  do {                                    \
    ASSERT_EQ(a.score, b.score);          \
    ASSERT_STREQ(a.filename, b.filename); \
  } while (0)

rating_entity_t NULL_RATING = {0, ""};

TEST(Rating, SimplePush) {
  rating_t rating = new rating_entity_t[RATING_SIZE]();
  rating_entity_t t1 = {1, "test"};

  push_to_rating(rating, &t1);

  ASSERT_EQ_RATING(rating[0], t1);
  ASSERT_EQ_RATING(rating[1], NULL_RATING);
  ASSERT_EQ_RATING(rating[2], NULL_RATING);
  ASSERT_EQ_RATING(rating[3], NULL_RATING);
  ASSERT_EQ_RATING(rating[4], NULL_RATING);

  delete[] rating;
}

TEST(Rating, FullPush) {
  rating_t rating = new rating_entity_t[RATING_SIZE]();
  rating_entity_t t1 = {1, "test"};

  for (size_t i = 0; i < RATING_SIZE; i++) push_to_rating(rating, &t1);

  for (size_t i = 0; i < RATING_SIZE; i++) ASSERT_EQ_RATING(rating[i], t1);

  delete[] rating;
}

TEST(Rating, PushSmaller) {
  rating_t rating = new rating_entity_t[RATING_SIZE]();
  rating_entity_t t1 = {5, "test"};
  rating_entity_t t2 = {1, "test"};

  for (size_t i = 0; i < RATING_SIZE; i++) push_to_rating(rating, &t1);

  push_to_rating(rating, &t2);

  for (size_t i = 0; i < RATING_SIZE; i++) ASSERT_EQ_RATING(rating[i], t1);

  delete[] rating;
}

TEST(Rating, IncorrectArgs) { ASSERT_NE(push_to_rating(NULL, NULL), 0); }
