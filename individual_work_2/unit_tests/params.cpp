#include "include/params.h"

#include <gtest/gtest.h>

TEST(GetParams, BadPointer) {
  int argc = 1;
  char **argv = nullptr;

  params_t *res = get_params(argc, argv);

  ASSERT_EQ(res, nullptr);

  free_params(res);
}

TEST(GetParams, NoDIR) {
  int argc = 3;
  char **argv = reinterpret_cast<char **>(malloc(sizeof(*argv) * argc));
  argv[0] = strdup("./app.example");

  argv[1] = strdup("-q");
  argv[2] = strdup("one two");

  params_t *res = get_params(argc, argv);

  ASSERT_NE(res, nullptr);

  ASSERT_EQ(res->help, 1);

  free_params(res);
  free_words(argv, argc);
}

TEST(GetParams, NoQuery) {
  int argc = 3;
  char **argv = reinterpret_cast<char **>(malloc(sizeof(*argv) * argc));
  argv[0] = strdup("./app.example");

  argv[1] = strdup("-d");
  argv[2] = strdup("./tests");

  params_t *res = get_params(argc, argv);

  ASSERT_NE(res, nullptr);

  ASSERT_EQ(res->help, 1);

  free_params(res);
  free_words(argv, argc);
}

TEST(GetParams, CorrectMinimal) {
  int argc = 5;
  char **argv = reinterpret_cast<char **>(malloc(sizeof(*argv) * argc));
  argv[0] = strdup("./app.example");

  argv[1] = strdup("-q");
  argv[2] = strdup("one two three");

  argv[3] = strdup("-d");
  argv[4] = strdup("./tests");

  params_t *res = get_params(argc, argv);

  ASSERT_NE(res, nullptr);

  ASSERT_EQ(res->help, 0);

  ASSERT_STREQ(res->dirname, "./tests");

  ASSERT_EQ(res->words_count, 3);
  ASSERT_STREQ(res->words[0], "one");
  ASSERT_STREQ(res->words[1], "two");
  ASSERT_STREQ(res->words[2], "three");

  ASSERT_EQ(res->log_level, 0);
  ASSERT_NE(res->proceses, 0);

  free_params(res);
  free_words(argv, argc);
}

TEST(GetParams, CorrectFull) {
  int argc = 9;
  char **argv = reinterpret_cast<char **>(malloc(sizeof(*argv) * argc));
  argv[0] = strdup("./app.example");

  argv[1] = strdup("-q");
  argv[2] = strdup("one two three");

  argv[3] = strdup("-d");
  argv[4] = strdup("./tests");

  argv[5] = strdup("-l");
  argv[6] = strdup("1");

  argv[7] = strdup("-p");
  argv[8] = strdup("1");

  params_t *res = get_params(argc, argv);

  ASSERT_NE(res, nullptr);

  ASSERT_EQ(res->help, 0);

  ASSERT_STREQ(res->dirname, "./tests");

  ASSERT_EQ(res->words_count, 3);
  ASSERT_STREQ(res->words[0], "one");
  ASSERT_STREQ(res->words[1], "two");
  ASSERT_STREQ(res->words[2], "three");

  ASSERT_EQ(res->log_level, 1);
  ASSERT_EQ(res->proceses, 1);

  free_params(res);
  free_words(argv, argc);
}

TEST(GetParams, UndefinedParam) {
  int argc = 6;
  char **argv = reinterpret_cast<char **>(malloc(sizeof(*argv) * argc));
  argv[0] = strdup("./app.example");

  argv[1] = strdup("-q");
  argv[2] = strdup("one two three");

  argv[3] = strdup("-d");
  argv[4] = strdup("./tests");

  argv[5] = strdup("--invalid");

  params_t *res = get_params(argc, argv);

  ASSERT_NE(res, nullptr);

  ASSERT_NE(res->help, 0);

  free_params(res);
  free_words(argv, argc);
}

TEST(GetParams, IncorrectProccessesNum) {
  int argc = 7;
  char **argv = reinterpret_cast<char **>(malloc(sizeof(*argv) * argc));
  argv[0] = strdup("./app.example");

  argv[1] = strdup("-q");
  argv[2] = strdup("one");

  argv[3] = strdup("-d");
  argv[4] = strdup("./tests");

  argv[5] = strdup("-p");
  argv[6] = strdup("byaka");

  params_t *res = get_params(argc, argv);

  ASSERT_NE(res, nullptr);

  ASSERT_EQ(res->help, 1);

  free_params(res);
  free_words(argv, argc);
}

TEST(GetParams, IncorrectLogLevel) {
  int argc = 7;
  char **argv = reinterpret_cast<char **>(malloc(sizeof(*argv) * argc));
  argv[0] = strdup("./app.example");

  argv[1] = strdup("-q");
  argv[2] = strdup("one");

  argv[3] = strdup("-d");
  argv[4] = strdup("./tests");

  argv[5] = strdup("-l");
  argv[6] = strdup("byaka");

  params_t *res = get_params(argc, argv);

  ASSERT_NE(res, nullptr);

  ASSERT_EQ(res->help, 1);

  free_params(res);
  free_words(argv, argc);
}
