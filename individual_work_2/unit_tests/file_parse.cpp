#include "include/file_parse.h"

#include <gtest/gtest.h>

#include "include/params.h"

#define TEMP_FILE_NAME "temp.out"
#define CREATE_FILE(file_name, string)       \
  do {                                       \
    file_name = fopen(TEMP_FILE_NAME, "w+"); \
    ASSERT_TRUE(file_name);                  \
    fprintf(file_name, "%s", string);        \
    fflush(file_name);                       \
    fseek(file_name, 0, SEEK_SET);           \
  } while (0)

TEST(GetWORD, simple) {
  FILE *file = NULL;
  CREATE_FILE(file, "one two three");

  char *word;
  word = get_word(file);
  ASSERT_STREQ(word, "one");
  free(word);

  word = get_word(file);
  ASSERT_STREQ(word, "two");
  free(word);

  word = get_word(file);
  ASSERT_STREQ(word, "three");
  free(word);

  word = get_word(file);
  ASSERT_EQ(word, nullptr);

  fclose(file);
}

TEST(GetWordsOccurance, OnlyOneWord) {
  FILE *file = NULL;
  CREATE_FILE(file, "one two three");
  fclose(file);

  int count = 1;
  char **words = reinterpret_cast<char **>(malloc(sizeof(*words) * count));
  words[0] = strdup("one");

  ASSERT_EQ(get_words_occurrence(TEMP_FILE_NAME, words, count), 1);

  free_words(words, count);
}

TEST(GetWordsOccurance, Simple) {
  FILE *file = NULL;
  CREATE_FILE(file, "one one two one");
  fclose(file);

  int count = 1;
  char **words = reinterpret_cast<char **>(malloc(sizeof(*words) * count));
  words[0] = strdup("one");

  ASSERT_EQ(get_words_occurrence(TEMP_FILE_NAME, words, count), 3);

  free_words(words, count);
}

TEST(GetWordsOccurance, ManyWords) {
  FILE *file = NULL;
  CREATE_FILE(file, "one one two one");
  fclose(file);

  int count = 2;
  char **words = reinterpret_cast<char **>(malloc(sizeof(*words) * count));
  words[0] = strdup("one");
  words[1] = strdup("two");

  ASSERT_EQ(get_words_occurrence(TEMP_FILE_NAME, words, count), 4);

  free_words(words, count);
}
