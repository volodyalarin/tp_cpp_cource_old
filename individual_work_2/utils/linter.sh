SOURCES_DIRS="./include ./src ./unit_tests"

cpplint --filter="-legal/copyright" --repository=./ --root=./  --recursive $SOURCES_DIRS
cppcheck $SOURCES_DIRS