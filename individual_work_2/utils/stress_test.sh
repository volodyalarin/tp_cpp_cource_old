mkdir tests
echo Генерация случайных данных
python3 ./utils/generate_random_data.py
cd ./build
make

echo Запуск параллельной реализации с принудительными 4-мя процессами
./main.out -d ../tests -q one -p 4 > temp1.out
echo Запуск последовательной реализации
./main.out -d ../tests -q one -p 1 > temp2.out

diff ./temp1.out ./temp2.out

if [ $? -eq 0 ]
then
    echo "Вывод совпадает"
    rm -rf ../tests

else
    echo "Тест провален"
    rm -rf ../tests
    exit 1
fi


