"""
Данный скрипт создаёт случайные тестовые данные
"""

import random

count = 500

# Участвующие слова в поиске
words = ["one", "two", "three", "four", "five"]

lorem = "Lorem ipsum dolor sit amet consectetur adipiscing elit Vivamus placerat augue ut mi congue sed tempor quam porttitor Sed eget eros non velit volutpat suscipit bibendum in mi Sed facilisis pulvinar est ac sollicitudin Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nulla aliquam leo a tincidunt tristique Nullam nec lectus viverra commodo tortor ut elementum massa Vestibulum tortor ligula mollis sit amet mauris quis aliquet facilisis orci Sed non ultricies nibh Praesent non mauris ac quam facilisis vestibulum Duis rutrum sit amet massa eget accumsan Aenean et sagittis lacus id porta lorem Integer congue urna sed ultricies molestie felis tellus molestie nibh sit amet posuere velit lectus id turpis Duis vehicula ultricies diam eget mollis ex gravida ac Nullam ac purus lorem Donec vulputate enim velit at semper quam pretium id Phasellus scelerisque massa et finibus sodales enim metus ultrices sapien non lacinia turpis nibh sit amet turpis Integer quis metus et mi rhoncus ornare Nulla auctor nec sapien sit amet porta Curabitur et dui vel dolor consectetur dignissim Quisque a arcu eu purus iaculis venenatis "

# Шум
random_words = lorem.split()


for i in range(count):
    if i % 100 == 0:
        print(i, "of", count)
    data = [(word, random.randint(1000, 80000)) for word in words]

    buffer = []

    name = "./tests/"
    for word, size in data:
        for i in range(size):
            buffer.append(word)
        name = name + word + str(size)
    
    for i in range(random.randint(100, 100000)):
        buffer.append(random.choice(random_words))

    for i in range(random.randint(100, 100000)):
        buffer.append("\n")

    name = name + ".txt"


    random.shuffle(buffer)

    file = open(name, "w")
    file.write(" ".join(buffer))
    file.close()
