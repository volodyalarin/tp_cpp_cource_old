#ifndef INCLUDE_FILE_PARSE_H_
#define INCLUDE_FILE_PARSE_H_

#include <stdio.h>

#define WORD_MAX_SIZE 256
#define WORD_SPECIFICATOR "%255s"

#ifdef __cplusplus
extern "C" {
#endif

char *get_word(FILE *file);

ssize_t get_words_occurrence(const char *filename, char **words, size_t count);

#ifdef __cplusplus
}
#endif

#endif  // INCLUDE_FILE_PARSE_H_
