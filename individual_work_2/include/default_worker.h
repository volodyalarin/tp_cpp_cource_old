#ifndef INCLUDE_DEFAULT_WORKER_H_
#define INCLUDE_DEFAULT_WORKER_H_

#include <dirent.h>

#include "include/params.h"
#include "include/rating.h"

int default_worker(params_t *params, struct dirent **file_list,
                   int number_of_files, rating_t rating);

#endif  // INCLUDE_DEFAULT_WORKER_H_
