#ifndef INCLUDE_IO_H_
#define INCLUDE_IO_H_

#include <dirent.h>
#include <semaphore.h>

#include "include/rating.h"

typedef struct help_option {
  char *long_name;
  char short_name;
  char *description;
  int is_required;
} help_option_t;

int print_rating(rating_t rating);

char *get_path(const char *dirname, const char *filename);

void free_file_list(struct dirent **file_list, int number_of_files);

void show_help();

#endif  // INCLUDE_IO_H_
