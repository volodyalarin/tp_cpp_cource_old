#ifndef INCLUDE_PROCCESS_WORKER_H_
#define INCLUDE_PROCCESS_WORKER_H_

#include <dirent.h>
#include <sys/mman.h>

#include "include/params.h"
#include "include/rating.h"

#define PROT_RW PROT_READ | PROT_WRITE
#define MAP_SH_AN MAP_SHARED | MAP_ANONYMOUS

int proccess_worker(params_t *params, struct dirent **file_list,
                    int number_of_files, rating_t rating);

#endif  // INCLUDE_PROCCESS_WORKER_H_
