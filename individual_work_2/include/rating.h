#ifndef INCLUDE_RATING_H_
#define INCLUDE_RATING_H_

#include <stdlib.h>

#define RATING_SIZE 5

typedef struct rating_entity {
  size_t score;
  char filename[255];
} rating_entity_t, *rating_t;

#ifdef __cplusplus
extern "C" {
#endif

int push_to_rating(rating_t buffer, const rating_entity_t* rating);

#ifdef __cplusplus
}
#endif

#endif  // INCLUDE_RATING_H_
