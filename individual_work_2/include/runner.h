#ifndef INCLUDE_RUNNER_H_
#define INCLUDE_RUNNER_H_

#include <dirent.h>
#include <semaphore.h>

#include "include/params.h"
#include "include/rating.h"

int runner(params_t *params, size_t *cur_file, struct dirent **file_list,
           rating_t rating, sem_t *sem_file, sem_t *sem_rating);

#endif  // INCLUDE_RUNNER_H_
