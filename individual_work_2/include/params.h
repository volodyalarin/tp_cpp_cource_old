#ifndef INCLUDE_PARAMS_H_
#define INCLUDE_PARAMS_H_

#include <stdlib.h>

typedef struct params {
  char *dirname;
  size_t proceses;
  size_t log_level;
  int help;
  char **words;
  size_t words_count;
} params_t;

#ifdef __cplusplus
extern "C" {
#endif

params_t *get_params(size_t argc, char **argv);
void free_params(params_t *params);

char **get_words(char *line, size_t *count);
void free_words(char **words, size_t count);

#ifdef __cplusplus
}
#endif

#endif  // INCLUDE_PARAMS_H_
